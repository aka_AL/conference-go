from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    params = {
        "query": f"{city} {state}",
        "page": 1,
        "per_page": 1,
    }
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    results = requests.get(url, headers=headers, params=params)
    data = json.loads(results.content)
    try:
        picture_url = data["photos"][0]["src"]["original"]
        return {"picture_url": picture_url}
    except KeyError:
        return {"picture_url": None}


def get_weather_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city},{state},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    results = requests.get(url, params=params)
    data = results.json()
    lat, lon = data[0]["lat"], data[0]["lon"]

    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    results = requests.get(url, params=params)
    data = results.json()
    if data == {"cod": "400", "message": "wrong latitude"}:
        return None
    else:
        temperature = data["main"]["temp"]
        description = data["weather"][0]["description"]
        return {"temperature": temperature, "description": description}
